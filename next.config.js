const isProd = process.env.NODE_ENV === "production";
// const isProd = false;

module.exports = {
  // assetPrefix: isProd ? "/riju" : "",
  // assetPrefix: "/riju",
  basePath: isProd ? "/riju" : "",
  reactStrictMode: true,
  trailingSlash: isProd ? true : false,
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Important: return the modified config
    // config.resolve.modules.push(path.resolve("node_modules"));
    // config.resolve.modules.push("node_modules");
    // config.resolve.alias.vscode = require.resolve(
    //   "monaco-languageclient/lib/vscode-compatibility"
    // );
    if (!isServer) {
      // config.node.net = "empty";
      config.resolve.fallback.net = false;
    }
    config.resolve.alias.vscode = require.resolve(
      "monaco-languageclient/lib/vscode-compatibility"
    );

    return config;
  },
};
