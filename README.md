![home](https://user-images.githubusercontent.com/6770106/133267560-0eea8701-ac94-45f8-8ba8-eead83d83622.png)

![editor](https://user-images.githubusercontent.com/6770106/133267600-85073cdc-9ebf-4b7c-bbb8-f728318b26a5.png)


This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.
